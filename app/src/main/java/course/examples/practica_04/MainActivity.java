package course.examples.practica_04;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Resources resources = getResources();

        TabHost tabHost=(TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec section;

        section=tabHost.newTabSpec("tab1");
        section.setContent(R.id.tab1);
        section.setIndicator("Temperatura");
        tabHost.addTab(section);
        section=tabHost.newTabSpec("tab2");
        section.setContent(R.id.tab2);
        section.setIndicator("Área");
        tabHost.addTab(section);
        section=tabHost.newTabSpec("tab3");
        section.setContent(R.id.tab3);
        section.setIndicator("Distancia");
        tabHost.addTab(section);

        tabHost.setCurrentTab(0);
    }

    public void calcularArea(View view){
        Intent intent = new Intent(this, area.class);
        EditText editText = (EditText) findViewById(R.id.medida);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void conversorDistancia(View view){
        Intent intent = new Intent(this, distancia.class);
        EditText editText = (EditText) findViewById(R.id.distancia);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void conversorGrados(View view){
        Intent intent = new Intent(this, temperatura.class);
        EditText editText = (EditText) findViewById(R.id.grados);
        String message = editText.getText().toString();
        message=Double.toString(((Double.parseDouble(message))-32)/1.8);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
